"""
Title: Simple Calculator

Type: Small exercise
"""

# Imports
import time
import os


#Variables...
calcresult=0

os.system('clear')

print ("What do you want to do today?")
print ("M to multiply, S to subtract, A to add and D to divide")
mode=raw_input()

print ("Type the first number:")

num1=input()

print ("Now type the second number:")

num2=input()

#Multiplication
if (mode == "M"):
    calcresult=num1*num2
    print calcresult

#Division
elif (mode == "D"):
    calcresult=num1/num2
    print calcresult
#Subtraction
elif (mode == "S"):
    calcresult=num1-num2
    print calcresult

#Addition
elif (mode == "A"):
    calcresult=num1+num2
    print calcresult
