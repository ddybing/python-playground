from tkinter import *
import tkinter.messagebox as box


root = Tk()
root.title('First window')


# Functions
def change_background_color():
    if root.cget('bg') == 'orange':
        root.configure(bg = 'purple')
    else:
        root.configure(bg = 'orange')


def show_infobox():
    box.showinfo("Infobox", 'This is an infobox')

def show_errorbox():
    box.showerror("Errorbox", 'This is an errorbox')

def show_warningbox():
    box.showwarning("Warning", 'This is a warning box')

label1 = Label(root, text="This is a label.")
label1.pack(padx = 200, pady = 50)

btn_showinfo = Button(root, text="Infobox", command=show_infobox)
btn_showinfo.pack(padx = 150, pady = 20)

btn_showerror = Button(root, text="Errorbox", command=show_errorbox)
btn_showerror.pack(padx = 200, pady = 20)

btn_showwarning = Button(root, text="Warningbox", command=show_warningbox)
btn_showwarning.pack(padx = 250, pady = 20)

btn_changebg = Button(root, text="Change bg!", command=change_background_color)
btn_changebg.pack(padx = 100, pady = 20)




root.mainloop()
