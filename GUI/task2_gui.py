

from tkinter import *


root = Tk()
root.title('First window')


btn_1 = Button(root, text="Button 1")
btn_1.grid(row=0)

btn_2 = Button(root, text="Button 2")
btn_2.grid(row=1)

btn_3 = Button(root, text="Button 3")
btn_3.grid(row=2)

btn_4 = Button(root, text="Button 4")
btn_4.grid(row=0, column=1)

btn_5 = Button(root, text="Button 5")
btn_5.grid(row=1, column=1)

btn_6 = Button(root, text="Button 6")
btn_6.grid(row=2, column=1)

btn_7 = Button(root, text="Button 7")
btn_7.grid(row=0, column=2)

btn_8 = Button(root, text="Button 8")
btn_8.grid(row=1, column=2)

btn_9 = Button(root, text="Button 9")
btn_9.grid(row=2, column=2)


root.mainloop()
