"""
Title: Gearbox Simulator
Type: Small project
Description: Small program that simulates a gearbox and it output shafts.
"""

"""
NOTE TO SELF

Key 39 = Up
Key 40 = Down
Key 83 = S

"""


import os
from time import sleep

# Clear screen
os.system('clear')

key = ord(getch())
enginerpm = 0
gear1rpm = 0
gear2rpm = 0
gear3rpm = 0
gear4rpm = 0
gear5rpm = 0
outputrpm = 0

while True:
    os.system('clear')s

    print ('Engine RPM: {}').format(enginerpm)
    print ('Gear 1 RPM: {}').format(gear1rpm)
    print ('Gear 2 RPM: {}').format(gear2rpm)
    print ('Gear 3 RPM: {}').format(gear3rpm)
    print ('Gear 4 RPM: {}').format(gear4rpm)
    print ('Gear 5 RPM: {}').format(gear5rpm)
    sleep(1)
