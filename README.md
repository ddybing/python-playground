# Python Playground
This is a playground for all of my Python exercises.

Please feel free to use any code for your own exercises or projects.

NOTE: Most programs are primarily written to run on *NIX 

Current Python programs: 

## Guess the Number
Your typical game. The computer generates a random number, with a value of 10 to 100, and your task is to guess what it is. 

## Is It Online
A small network application to check if devices are online. This works for local network devices only, only IPv4 enabled devices and only based on IP-addresses.
Support for lookup based on MAC-address or IPv6 addresses may be added in the future. 

## Memory Tile Game
A memory tile game created as a task at school. The memory tile game will, at the moment, only run properly on Windows as it depends on a
sound module which is only available in Windows operating systems.
The idea of the memory tile game is that you are shown a board of cards/tiles. You choose a tile and are presented with its value. Then, you choose another tile and its value is presented as well. If those cards match, you get a point. If they do not match, you need to remember their values, so that you can match equals cards later. Two and two cards are equal. 

## Quiz 1
A small quiz.

## Quiz 2
A small quiz. 


## Shopping list
Very basic shopping list that takes 10 items, puts it in a list and then prints the list on the user's screen. 

## Simple Socket Server
Small socket server that accepts telnet connections. 
