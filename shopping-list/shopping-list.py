"""
Title: Shopping list
Type: Small project
Description: The program will ask the user to create a shopping list of 10 items. It will then print that list to the user.
"""

import os

os.system('clear')
print ('Welcome to Input List. Let us create your shopping list')
print ('Please type up to 10 items, seperated by a line shift(ENTER)')

shoppinglist=[]
itemscount=0

# Counting up to ten items. Adds item to list and adds +1 to the counter.
while itemscount < 10:
	print ('Please enter a new item')
	item=raw_input()
	shoppinglist.append(item)
	itemscount = itemscount + 1

# Print each item in a vertical list.
for item in shoppinglist:
	print ('* {}'.format(item))
