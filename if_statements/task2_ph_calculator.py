import os

# Clear console (on Windows).
os.system('cls')


while True:
    inputph = float(input("Please enter a pH value between 0 and 14: "))
    if (0.0 <= inputph <= 14.0):
        print ('Number is a valid pH value')
        if (inputph <= 6.5):
            print ('pH value is: Acid')
        elif (6.6 <= inputph <= 7.3):
            print ('pH value is: Neutral')
        elif (7.4 <= inputph <= 14.0):
            print ('pH value is: Alkaline')
