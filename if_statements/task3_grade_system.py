import os

# Clear console (on Windows).
os.system('cls')


while True:
    gradetype = input('Please type L for letters or P for percentage scores: ')
    if (gradetype == 'L'):
        gradeinput = input('Please enter your grade as a letter: ')

        if (gradeinput == 'F'):
            print ('You\'ve entered {}. This grade is the same as percentage scores below 60.'.format(gradeinput))
        elif (gradeinput == 'D-'):
            print ('You\'ve entered {}. This grade is the same as percentage scores of 60 to 62.'.format(gradeinput))
        elif (gradeinput == 'D'):
            print ('You\'ve entered {}. This grade is the same as percentage scores of 63 to 66.'.format(gradeinput))
        elif (gradeinput == 'D+'):
            print ('You\'ve entered {}. This grade is the same as percentage scores of 67 to 69.'.format(gradeinput))
        elif (gradeinput == 'C-'):
            print ('You\'ve entered {}. This grade is the same as percentage scores of 70 to 72.'.format(gradeinput))
        elif (gradeinput == 'C'):
            print ('You\'ve entered {}. This grade is the same as percentage scores of 73 to 76.'.format(gradeinput))
        elif (gradeinput == 'C+'):
            print ('You\'ve entered {}. This grade is the same as percentage scores of 77 to 79.'.format(gradeinput))
        elif (gradeinput == 'B-'):
            print ('You\'ve entered {}. This grade is the same as percentage scores of 80 to 82.'.format(gradeinput))
        elif (gradeinput == 'B'):
            print ('You\'ve entered {}. This grade is the same as percentage scores of 83 to 86.'.format(gradeinput))
        elif (gradeinput == 'B+'):
            print ('You\'ve entered {}. This grade is the same as percentage scores of 87 to 89.'.format(gradeinput))
        elif (gradeinput == 'A-'):
            print ('You\'ve entered {}. This grade is the same as percentage scores of 90 to 92.'.format(gradeinput))
        elif (gradeinput == 'A'):
            print ('You\'ve entered {}. This grade is the same as percentage scores of 93 to 96.'.format(gradeinput))
        elif (gradeinput == 'A+'):
            print ('You\'ve entered {}. This grade is the same as percentage scores of 97 to 100.'.format(gradeinput))

        else:
            print ('It looks like you\'ve entered an invalid grade score.')

    elif (gradetype == 'P'):
        gradeinput = int(input('Please enter your grade as a percentage score: '))
        if (0 <= gradeinput <= 59):
            print ('You\'ve entered {}. This score is the same as grade F'.format(gradeinput))
        elif (60 <= gradeinput <= 62 ):
            print ('You\'ve entered {}. This score is the same as grade D-'.format(gradeinput))
        elif (63 <= gradeinput <= 66):
            print ('You\'ve entered {}. This score is the same as grade D'.format(gradeinput))
        elif (67 <= gradeinput <= 69):
            print ('You\'ve entered {}. This score is the same as grade D+'.format(gradeinput))
        elif (70 <= gradeinput <= 72):
            print ('You\'ve entered {}. This score is the same as grade C-'.format(gradeinput))
        elif (73 <= gradeinput <= 76):
            print ('You\'ve entered {}. This score is the same as grade C'.format(gradeinput))
        elif (77 <= gradeinput <= 79):
            print ('You\'ve entered {}. This score is the same as grade C+'.format(gradeinput))
        elif (80 <= gradeinput <= 82):
            print ('You\'ve entered {}. This score is the same as grade B-'.format(gradeinput))
        elif (83 <= gradeinput <= 86):
            print ('You\'ve entered {}. This score is the same as grade B'.format(gradeinput))
        elif (87 <= gradeinput <= 89):
            print ('You\'ve entered {}. This score is the same as grade B+'.format(gradeinput))
        elif (90 <= gradeinput <= 92):
            print ('You\'ve entered {}. This score is the same as grade A-'.format(gradeinput))
        elif (93 <= gradeinput <= 96):
            print ('You\'ve entered {}. This score is the same as grade A'.format(gradeinput))
        elif (97 <= gradeinput <= 100):
            print ('You\'ve entered {}. This score is the same as grade A+'.format(gradeinput))

        else:
            print ('It looks like you\'ve entered an invalid grade score.')
