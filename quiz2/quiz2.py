"""
Title: Quiz2
Type: Small Exercise Project
Description: Short quiz containg six questions, one try per question.
"""


import os
from time import sleep
import sys

# Type writer stuff

title = '''
 ____ ____ ____ ____ ____
||Q |||U |||I |||Z |||2 ||
||__|||__|||__|||__|||__||
|/__\|/__\|/__\|/__\|/__\|

'''

greeting1 = "Welcome to Quiz 2. Each category presented contains six questions, and you will be given one try per question - so make sure you get it right the first time!"

quizCategoriesTable = '''
_________________________
| Number |   Category   |
|-----------------------|
|   1    |  Technology  |
|   2    |  History     |
|   3    |  Science     |
|   4    |  Filmography |
|-_-_-_-_-_-_-_-_-_-_-_-|
'''



# Variables and lists
technologyQuestions = ['What will eventually replace the old IPv4 standard?', 'What transfer speed is correct for USB 2.0?', 'What does the NT in Windows NT stand for?', 'EXT is especially common within one family of operating systems - which?', 'You have a file that has a size of 110 bytes. How many bits are there in this file?', 'CAT5e is capable of delivering speeds up to 1Gbit/s. But what is its maximum distance?' ]
technologyAnswers = ['IPv6', '480Mbps', 'New Technology', 'Linux', '880', '100 meters' ]

historyQuestions = ['In which year was the Norwegian constitution written?', 'Who was the president of the United States between 1961-1963?', '']
historyAnswers = ['']

scienceAnswers = ['']

filmographyQuestions = ['What movie universe feautures the droid R2-D2 and the Jedi Skywalker?', 'Who plays the role of the main character on the show "Breaking Bad"?', "James Cameron's 'Titanic' is well known for its many soundtracks. Who composited them?", ]
filmographyAnswers = ['Star Wars', 'Bryan Cranston', 'James Horner']


os.system('clear')

# ASCII Art "loading" animation.



for char in title:
    sleep(0.05)
    sys.stdout.write(char)
    sys.stdout.flush()

for char in greeting1:
    sleep(0.05)
    sys.stdout.write(char)
    sys.stdout.flush()

for char in quizCategoriesTable:
    sleep(0.05)
    sys.stdout.write(char)
    sys.stdout.flush()

whichQuiz = raw_input("Please tell me what quiz you would like to play: ")

if (whichQuiz == '1'):
    print ("Category 1 - Technology - it is!")

elif (whichQuiz == '2'):
    print ("Category 2 - History - it is!")

elif (whichQuiz == '3'):
    print ("Category 3 - Science - it is!")

elif (whichQuiz == '4'):
    print ("Category 4 - Filmography - it is!")
