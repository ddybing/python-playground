"""
Title: Guess The Number
Type: Small Exercise Project
"""

import random
import sys
import os 
from time import sleep


# Variables
greeting=('Welcome! Get ready to guess a number between 10-100.')



numberOfTries=0 # Keeps track of how many times the user has tried!"
# Clears the screen when the game starts
os.system('clear')

"""
os.system('figlet Guess The Number') <- Commented until I find a better solution to do this
without figlet. 
"""

# Simulates the effect of text being typed by a typewriter.
for char in greeting:
	sleep (0.05)
	sys.stdout.write(char)
	sys.stdout.flush()

def hello_world():
	print ('Hello')
	raw_input()

sleep (2)
os.system('clear')
number = random.randint(10,100)

while numberOfTries <10:
	print('Now take a guess!')
	guess = input()
	guess = int(guess)
	numberOfTries = numberOfTries + 1
	if guess < number:
		print ('Sorry, your guess it too low!')
		
	if guess > number:
		print ('Sorry, your guess it too high!')
	if guess == number:
		print ('Congratulations!!!!!!!!!!')
		print ('Game will not exit...')
		break	

