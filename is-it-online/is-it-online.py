"""
Title: Is it Online
Type: Small Project
Description: A small program to check and monitor whether a device(user) is online or not

# NOTE: Will not work on IPv6 networks...

"""
import curses
from time import sleep
import sys
import os
import socket
from ConfigParser import SafeConfigParser


# Must add more colors to this class...
class textColors :
	GREEN = '\033{92m'
	BLUE = '\033[94m'


config = SafeConfigParser()
"""
config.read ('config.ini')
config.add_section('hosts')
config.set('hosts', 'gateway', '192.168.100.1')
config.set('hosts', 'laptop', '192.168.100.28')
config.set('hosts', 'workstation', '192.168.100.90')

with open('config.ini','w') as f:
	config.write(f)

"""
networkUp = 0

os.system('clear')

print ('*** IS IT ONLINE is starting up... ***\n\n')

hostexamples = ['192.168.100.1', '8.8.8.8', '192.168.100.28', '192.168.100.100']


# Check for network connection...
while (networkUp == 0):
	print ('Checking for network connection...')
	sleep(1)
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	s.connect(('192.168.100.1', 80))
	localIP = (s.getsockname()[0])
	print ('Your private IP address seems to be:\033[92m {} \033[0m').format(localIP)
	print ('\033[92mYou are connected to a network!\033[0m')
	s.close()
	networkUp = 1
"""
if os.path.isfile('./config.ini'):
	print ('Existing config file found. (Type) Load or overwrite?')
	configAnswer = raw_input()


else:
	print ('No config file found. Creating new config file. Just a sec...')
	sleep(3)
"""
# Load config
config.read('config.ini')

"""
Need to find a way to get all content of section 'hosts'

"""

# Check status of hosts...
print ('Checking hosts...\n\n')

for host in hostexamples:
	response = os.system('ping -c 1 {} > /dev/null'.format(host))

	# If host is up
	if (response == 0):
		print ('[\033[92mUP\033[0m]') + ('Host {} '.format(host))

	#If host is down
	else:
		print ('[\033[31mDOWN\033[0m]') + ('Host {} '.format(host))
