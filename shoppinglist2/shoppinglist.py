import os
# Clear the console (on Windows)
os.system('cls')

shopping_list = []

print ('Type the item you want to add and press Enter. \nTo print list, type End and then press Enter.')

while True:
    shopping_item = input('Enter a new item to add to the list: ')
    if (shopping_item == 'End'):
        break
    shopping_list.append(shopping_item)
    print (str(len(shopping_list)) + ' items in shopping list.\n')


print (*shopping_list, sep = '\n')
