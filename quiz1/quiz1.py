"""
Title: Quiz1
Type: Small Exercise Project
Description: Short quiz with unlimited retries
"""
import os

os.system('clear')
print ('Welcome to my little quiz')
print ('Let us start!')
correctAnswers=0
question1Answered = 0
question2Answered = 0
question3Answered = 0
question4Answered = 0

while (question1Answered == 0):
	print ('Question 1')
	print ('On what contintent is India situated?')
	print ('')
	print ('')
	print ('a. Europe')
	print ('b. North America')
	print ('c. Oceania')
	print ('d. India')
	answer = raw_input('What is your answer?')
	if (answer == 'd'):
		print ('Congratulations - that is correct!')
		question1Answered = 1
		correctAnswers = correctAnswers + 1
	else:
		print ('Sorry, try again!')

while (question2Answered == 0):
	os.system('clear')
	print ('Number of correct answers: {}/5'.format(correctAnswers))
	print ("You're doing great! Now let's try a new quiz")
	print ('')
	print ('Question 2')
	print ('Steve Jobs was the CEO of a tech company for more than 10 years. What company was that?')
	print ('')
	print ('')
	print ('a. Microsoft')
	print ('b. Apple')
	print ('c. Google')
	print ('d. Yahoo')
	answer = raw_input('What is your answer?')
	if (answer == 'b'):
		print ('Congratulations - that is correct!')
		question2Answered = 1
	else:
		print
