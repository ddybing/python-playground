"""
Title: Simple Socket server
Description: Small socket / telnet server exercise.
"""


#Imports
import socket
import sys


# Ask for IP and port to bind.
HOST = raw_input("Binding IP: ")
PORT = input("Binding Port: ")
MESSAGE = "I've been expecting you..."


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print "Socket created..."

# Try to bind socket to IP and port
try:
    s.bind((HOST, PORT))
except socket.error as msg:
    print 'Bind failed!'
    sys.exit()

print 'Socket bind complete; IP: {} PORT: {}'.format(HOST, PORT)

s.listen(10)
print 'I am now listening...'

# keep connection

while True:
    conn, addr = s.accept()
    data = conn.recv(20)
    print 'Connected with client!'
    print "Received data:", data
    conn.send(data) #Echo
    data = 0

s.close()
