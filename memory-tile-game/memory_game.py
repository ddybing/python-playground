#!/usr/bin/env python


from time import sleep
import threading
import random
from sys import platform
import sys
import os
import winsound


# --- Global Variables, Lists and Tuples ---



file_list = ['audio/hooray.wav','audio/opening.wav','audio/player1.wav','audio/player2.wav']



# Class of terminal colors

class colors:
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    ENDCOLOR = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


# Check whether the computer is running Linux, Windows or macOS/OS X to know
# sound module to use.
def checkOS():
    if (platform == "linux2") or (platform == "linux"):
        os.system('clear')
        print ('Current operating system is Linux, setting appropriate sound module and other paramters.')
        soundModule = 'linux'

    elif (platform == "win32") or (platform == "cygwin"):
        os.system('cls')
        print ('Current operating system is Windows, setting appropriate sound module and other parameters.')
        return 'cls'

    elif (platform == "darwin"):
        os.system('clear')
        print ('Current operating system is macOS / Mac OS X, sound is not supported yet!')
        soundModule = ''
        screenclear = '"clear"'

    else:
        print ('Operating system could not be detected, no sound will be used.')



# Check that all files are available and in their correct location.
def checkIntegrity():
    missing_count = 0
    print ('Checking for missing files...')
    for i in file_list:
        if (os.path.isfile(i) == False):
            print (colors.WARNING + 'WARNING: File {} is missing...'.format(i) + colors.ENDCOLOR)
            missing_count = +1
        if (os.path.isfile(i) == True):
            print (colors.OKBLUE + 'File {} exists...'.format(i) + colors.ENDCOLOR)
    if (missing_count == 0):
        print (colors.OKGREEN + '\nAll files are present, no files missing!' + colors.ENDCOLOR)
    sleep(3)

def greeting():
    # Greeting...
    os.system(screenclear)
    greetingcountdown = 5
    title = """
  __  __                          _____ _ _        ___
 |  \/  |___ _ __  ___ _ _ _  _  |_   _(_) |___   / __|__ _ _ __  ___
 | |\/| / -_) '  \/ _ \ '_| || |   | | | | / -_) | (_ / _` | '  \/ -_)
 |_|  |_\___|_|_|_\___/_|  \_, |   |_| |_|_\___|  \___\__,_|_|_|_\___|
                           |__/
    """
    for char in title:
        sleep(0.01)
        sys.stdout.write(colors.OKGREEN + char + colors.ENDCOLOR)
        sys.stdout.flush()
    sleep(3)
    menu()


def greetingmusic():
    winsound.PlaySound('audio/opening.wav', winsound.SND_FILENAME)

screenclear = checkOS()








gamemode = 0

def hooray():
    winsound.PlaySound('audio/hooray.wav', winsound.SND_FILENAME)

def menu():
    global gamemode
    os.system(screenclear)
    print ('Welcome...\nPlease choose game mode.')
    print ('1 > Single Player\n2 > Multiplayer\n')
    gamemode = int(input('Enter mode: '))
    game1()

def choose_board():
    gameboard_list = ['BRJUGJULBUTRTLGU', 'JKJOQKWOEIYQEWYI', 'MNHMJFASNFDJHDSA', 'HJYSJKLOROHKYLRS',
    'VCUXXVZBCBTZURTR', 'QHOYFQGYDUOHUGFD']
    # Choose a random gameboard.
    random_board = random.choice(gameboard_list)
    return random_board




# Global stats Variables
tries = 0
equal_cards = 0
player = 1
result_p1 = 0
result_p2 = 0
player1_intro = False
player2_intro = False

def game1():

    def summary():
        os.system(screenclear)
        if (result_p1 > result_p2):
            print(colors.OKGREEN, 'Player 1', colors.ENDCOLOR, 'wins!' )
        if (result_p1 < result_p2):
            print(colors.OKGREEN, 'Player 2',colors.ENDCOLOR, 'wins!')
        elif (result_p1 == result_p2):
            print('It\'s a draw, Player 1 and Player 2 has the same number of equal cards.')
        else:
            pass
        sys.exit()



    os.system(screenclear)
    random_board = choose_board()
    template_dict = {
     'A1':None, 'A2':None, 'A3':None, 'A4':None,
     'B1':None, 'B2':None, 'B3':None, 'B4':None,
     'C1':None, 'C2':None, 'C3':None, 'C4':None,
     'D1':None, 'D2':None, 'D3':None, 'D4':None}

    gameboard_dict = dict(zip(template_dict, random_board))
    dynamic_gameboard = [
    'A1', 'A2', 'A3', 'A4',
    'B1', 'B2', 'B3', 'B4',
    'C1', 'C2', 'C3', 'C4',
    'D1', 'D2', 'D3', 'D4']

    def flip_card(user_guess1, user_guess2, tempcard1, tempcard2, dynamic_gameboard):
        card1_position = dynamic_gameboard.index(user_guess1)
        card2_position = dynamic_gameboard.index(user_guess2)
        dynamic_gameboard[card1_position] = tempcard1
        dynamic_gameboard[card2_position] = tempcard2

    def print_gameboard():
        global player
        os.system(screenclear)
        print('Player: {}'.format(player))
        print('--- Stats ---')
        print(colors.WARNING,'Tries: ', tries,'/ 30', colors.ENDCOLOR)
        print(colors.OKGREEN, 'Equal cards: ', equal_cards, '/ 16', colors.ENDCOLOR)
        print('--- Gameboard ---', end="\n")
        print(*dynamic_gameboard[0:4])
        print(*dynamic_gameboard[4:8])
        print(*dynamic_gameboard[8:12])
        print(*dynamic_gameboard[12:16])



    def draw_cards():
        global tries
        global gamemode
        global equal_cards
        global result_p1
        global result_p2
        global player
        global player1_intro
        global player2_intro

        if (player == 1) and (gamemode == 1) and (player1_intro == False):
            winsound.PlaySound('audio/player1.wav', winsound.SND_FILENAME)
            player1_intro = True
        elif (player == 1) and (gamemode == 2) and (player1_intro == False):
            winsound.PlaySound('audio/player1.wav', winsound.SND_FILENAME)
            player1_intro = True

        elif (player == 2) and (gamemode == 2) and(player2_intro == False):
            winsound.PlaySound('audio/player2.wav', winsound.SND_FILENAME)
            player2_intro = True
        elif (player == 2) and (gamemode == 1):
            summary()
        elif (player == 3) and (gamemode == 2):
            summary()


        if (tries <= 30):
            pass
        else:
            print('You\'ve used your 30 tries!')
            if (gamemode == 1 and player == 1):
                result_p1 = equal_cards
                summary()
            elif (gamemode == 2 and player == 1):
                result_p1 = equal_cards
                # Reset gameboard and stats
                equal_cards = 0
                tries = 0
                player += 1
                game1()
            elif (gamemode == 2 and player == 2):
                result_p2 = equal_cards
                player = 3
        if (equal_cards == 16):
            print('Good job, you\'ve matched all cards!')
            print('If you selected single player, you will be taken to the menu.')
            print('If you selected multi player, you will be taken to the next game.')
            sleep(3)
            if (gamemode == 1) and (player == 1):
                result_p1 = equal_cards
                summary()
            elif(gamemode == 2) and (player == 1):
                result_p1 = equal_cards
                equal_cards = 0
                tries = 0
                player += 1
                game1()
            elif(gamemode == 2) and (player == 2):
                result_p2 = equal_cards
                summary()
            else:
                pass

        else:
            pass


        print_gameboard()
        user_guess1 = input('\nDraw card 1: ')
        if user_guess1 in gameboard_dict:
            tempcard1 = gameboard_dict[user_guess1]
            print ('Card value is {}\n'.format(tempcard1))
            user_guess2 = input('Draw card 2: ')
        else:
            print('Invalid card')
            sleep(1)
            draw_cards()
        if user_guess2 in gameboard_dict:
            tempcard2 = gameboard_dict[user_guess2]
            print ('Card value is {}\n'.format(tempcard2))
        else:
            print('Invalid card')
            sleep(1)
            draw_cards()
        if (tempcard1 == tempcard2):
            print('Match!!')
            winsound.PlaySound('audio/hooray.wav', winsound.SND_FILENAME)
            equal_cards += 2
            tries += 1
            print ('Good job! You\'ve matched {} cards so far.'.format(equal_cards))
            sleep(2)
            flip_card(user_guess1, user_guess2, tempcard1, tempcard2, dynamic_gameboard)
            draw_cards()
        else:
            print('Cards do not match!')
            sleep(3)
            tries += 1
            draw_cards()

    draw_cards()


def game2():
    pass



checkOS()
checkIntegrity()

t1 = threading.Thread(target=greeting)
t2 = threading.Thread(target=greetingmusic)
t1.start()
t2.start()
